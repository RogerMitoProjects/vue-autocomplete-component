'use strict';

export default {
  getAll: () => {
    return [
      { label: 'Corvette', value: 1 },
      { label: 'Corola', value: 4 },
      { label: 'Ferrari', value: 2 },
      { label: 'Mustang', value: 3 },
    ];
  },
};
