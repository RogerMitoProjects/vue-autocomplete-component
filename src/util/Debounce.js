'use strict';
/**
 *  @description Slows down the function execution according to the time specified.
 * @param {Function} fn Target function to be delayed.
 * @param {Number} delay Wait time.
 */
module.exports = function(fn, delay) {
  let timeoutID = null;
  return function() {
    clearTimeout(timeoutID);
    const args = arguments;
    const that = this;
    timeoutID = setTimeout(() => {
      fn.apply(that, args);
    }, delay);
  };
};
